﻿using System;
using System.Net;
using System.Web.Http;

namespace EchoApi.Controllers
{
    public class EchoController : ApiController
    {
        [Route("echo/{input}")]
        public IHttpActionResult Get(string input)
        {
            return Content(HttpStatusCode.OK, input);
        }
    }
}